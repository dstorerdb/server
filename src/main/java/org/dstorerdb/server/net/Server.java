/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dstorerdb.common.model.User;
import org.dstorerdb.common.streams.StringInputStream;
import org.dstorerdb.common.streams.StringOutputStream;
import org.dstorerdb.common.system.SysInfo;
import org.dstorerdb.encryptor.encryptor.Encryptor;
import org.dstorerdb.logger.Log;
import org.dstorerdb.server.config.ConfigManager;
import org.dstorerdb.server.config.InfoManager;
import org.dstorerdb.server.net.thread.TClient;
import org.dstorerdb.server.net.thread.TRequestManager;
import org.dstorerdb.server.system.LogMessage;
import org.dstorerdb.server.system.MessagesManager;
import org.mpizutil.electrolist.structure.ElectroList;

/**
 *
 * @author martin
 */
public class Server extends Thread{
    private final ServerSocket serverSock;
    private final ElectroList<TClient> listClientsConnected;
    private final ConfigManager configManager;
    private final InfoManager infoManager;
    private static Server thisObj;

    private final MessagesManager msgManager;
    private Log log;

    public static final Encryptor SYS_ENCRYPTOR = new Encryptor(SysInfo.DEFAULT_KEY);
    
    public static Server getServer(){
        return thisObj;
    }
    
    public static void newInstance(){
        try {
            thisObj = new Server();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Server() throws IOException{
        serverSock = new ServerSocket(SysInfo.DEFAULT_PORT_1);
        listClientsConnected = new ElectroList<>();
        configManager = new ConfigManager();
        infoManager = new InfoManager();

        msgManager = MessagesManager.getInstance();
        log = Log.getLog(this);
    }
    
    public ConfigManager getConfigManager() {
        return configManager;
    }

    public InfoManager getInfoManager() {
        return infoManager;
    }
    
    public void addClient(Client client){
        TClient newCli = new TClient(client);
        newCli.start();
        listClientsConnected.add(newCli);
    }
    
    public void addClient(User user, Socket sock,
                          StringOutputStream os, StringInputStream is){
        TClient newCli = new TClient(new Client(user, sock, os, is));
        newCli.start();
        listClientsConnected.add(newCli);
    }
    
    public void disconnectAll(){
        for (TClient client : listClientsConnected)
            client.disconnectClient();
    }
    
    public void disconnectClient(long threadId) {
        int idClient = 0;
        for (TClient tClient : listClientsConnected) {
            if (tClient.getId() == threadId) {
                tClient.closeConnection();
                break;
            }
            idClient++;
        }
        listClientsConnected.remove(idClient);
    }
    
    @Override
    public void run(){
        log.info(msgManager.getProperty(LogMessage.SERVER_STARTED));
        while (true) {
            try {
                log.info(msgManager.getProperty(LogMessage.WAITING_CLIENTS));
                new TRequestManager(serverSock.accept()).start();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void main(String[] args) {
        Server.newInstance();
        Server.getServer().start();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.net.thread;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dstorerdb.cmdintepreter.cmd.BadSyntaxException;
import org.dstorerdb.cmdintepreter.cmd.CmdOrder;
import org.dstorerdb.cmdintepreter.cmd.Command;
import org.dstorerdb.cmdintepreter.cmd.Interpreter;
import org.dstorerdb.cmdintepreter.interfaces.InterpreterListener;
import org.dstorerdb.common.interfaces.Communicable;
import org.dstorerdb.common.model.User;
import org.dstorerdb.common.streams.StringInputStream;
import org.dstorerdb.common.streams.StringOutputStream;
import org.dstorerdb.common.system.ServerMessages;
import org.dstorerdb.encryptor.exceptions.InvalidTextException;
import org.dstorerdb.logger.Log;
import org.dstorerdb.server.net.Client;
import org.dstorerdb.server.net.Server;
import org.dstorerdb.server.net.interpreter.RQMInterpreterListener;
import org.dstorerdb.server.system.LogMessage;
import org.dstorerdb.server.system.MessagesManager;
import org.dstorerdb.server.system.UserManager;

/**
 *
 * @author martin
 */
public class TRequestManager extends Thread implements Communicable {
    private final Socket sockRequest;
    private final StringOutputStream outputStream;
    private final StringInputStream inputStream;
    private Interpreter requestInterpreter;

    private MessagesManager msgManager;
    private Log log;
    
    private boolean connected;
    
    //private static final byte EOF = -1;
    
    public TRequestManager(Socket sockRequest) throws IOException {
        this.sockRequest = sockRequest;
        outputStream = new StringOutputStream(sockRequest.getOutputStream());
        inputStream = new StringInputStream(sockRequest.getInputStream());
        requestInterpreter = new Interpreter();
        requestInterpreter.setListener(getInterListener());

        msgManager = MessagesManager.getInstance();
        log = Log.getLog(this);
        setName("TRequestManager: "+getId());
    }
    
    private InterpreterListener getInterListener(){
        return new RQMInterpreterListener(this);
    }

    public Interpreter getRequestInterpreter() {
        return requestInterpreter;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public Socket getSockRequest() {
        return sockRequest;
    }

    public StringOutputStream getOutputStream() {
        return outputStream;
    }

    public StringInputStream getInputStream() {
        return inputStream;
    }

    public MessagesManager getMsgManager() {
        return msgManager;
    }

    public void execCommand(String strCommand) throws BadSyntaxException {
        requestInterpreter.execCommand(new Command(strCommand));
    }
    
    public void sendResponseMessage(){
        sendData(requestInterpreter.getResponseMsg());
    }
    
    public void sendSyntaxErrorMessage(String msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(ServerMessages.ERROR_SYNTAX).append(ServerMessages.HEADER_SEPARATOR)
                .append(msg).append(ServerMessages.RESPONSE_MSG);
        sendData(sbMsg.toString());
        sbMsg.delete(0, sbMsg.length());
    }
    
    @Override
    public void sendData(String data) {
        try {
            outputStream.writeString(data);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(TRequestManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getReceivData() {
        try {
            // Mensaje sin el finalizador
            String msg = inputStream.readString();
            return msg.substring(0, msg.length()-4);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(TRequestManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    @Override
    public void run() {
        try {
            log.info(msgManager.getProperty(LogMessage.CLIENT_CONNECTED_FROM)
                    +sockRequest.getInetAddress().toString());
            String msg = getReceivData();
            log.info(msgManager.getProperty(LogMessage.REQUEST_FROM_CLIENT)
                    +sockRequest.getInetAddress().toString()+": "+msg);
            execCommand(msg);
            sendResponseMessage();
            log.info(msgManager.getProperty(LogMessage.RESPONSE_CLIENT)
                    +requestInterpreter.getResponseMsg());
        } catch (BadSyntaxException ex) {
            sendSyntaxErrorMessage(ex.getMessage());
        }
    }

}

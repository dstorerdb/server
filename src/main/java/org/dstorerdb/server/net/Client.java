/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.net;

import org.dstorerdb.cmdintepreter.cmd.Interpreter;
import org.dstorerdb.cmdintepreter.interfaces.InterpreterListener;
import org.dstorerdb.common.interfaces.Communicable;
import org.dstorerdb.common.model.User;
import org.dstorerdb.common.streams.StringInputStream;
import org.dstorerdb.common.streams.StringOutputStream;
import org.dstorerdb.common.system.ServerMessages;
import org.dstorerdb.encryptor.exceptions.InvalidTextException;
import org.dstorerdb.server.net.interpreter.ClientInterpreterListener;
import org.dstorerdb.server.system.UserManager;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.dstorerdb.common.system.ServerMessages.HEADER_SEPARATOR;

/**
 *
 * @author martin
 */
public class Client implements Communicable {
    private final User user;
    private final UserManager userManager;
    private final Socket socket;
    private final StringOutputStream outputStream;
    private final StringInputStream inputStream;
    private final Interpreter interpreter;
    
    private boolean connected;
    
    private static final StringBuilder sBuild = new StringBuilder();
    
    // Cada cliente tendra un UserManager con el que se gestionara cada operacion con su usuario correspondiente
    // Algo smilar a lo que hacia con el CloudManager.
    
    
    public Client(User user, Socket socket) throws IOException {
        this(user, socket, new StringOutputStream(socket.getOutputStream()), 
                new StringInputStream(socket.getInputStream()));
    }

    public Client(User user, Socket socket, StringOutputStream buffWriter, StringInputStream buffReader) {
        this.user = user;
        this.userManager = new UserManager(user);
        this.socket = socket;
        this.outputStream = buffWriter;
        this.inputStream = buffReader;
        this.interpreter = new Interpreter(getInterListener());
        connected = true;
        //System.out.println(socket.getInetAddress().getHostAddress());
    }
    
    private InterpreterListener getInterListener(){
        return new ClientInterpreterListener(this);
    }
    
    private void buildMessage(String header, Object msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(header).append(HEADER_SEPARATOR).append(msg.toString())
                .append(ServerMessages.RESPONSE_MSG);
        interpreter.setResponseMsg(sbMsg.toString());
        //System.out.println("Client: InterpreterMessage: "+interpreter.getResponseMsg());
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getRemoteAddress(){
        return socket.getInetAddress().getHostAddress();
    }
    
    public void closeStreams(){
        try {
            socket.shutdownOutput();
            socket.shutdownInput();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeSocket(){
        try {
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConnection(){
        closeStreams();
        closeSocket();
    }

    public User getUser() {
        return user;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public Interpreter getInterpreter() {
        return interpreter;
    }

    @Override
    public void sendData(String data) {
        try {
            outputStream.writeString(data);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getReceivData() {
        try {
            return inputStream.readString();
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}







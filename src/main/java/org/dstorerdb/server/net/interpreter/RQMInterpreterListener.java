package org.dstorerdb.server.net.interpreter;

import org.dstorerdb.cmdintepreter.cmd.CmdOrder;
import org.dstorerdb.cmdintepreter.cmd.Command;
import org.dstorerdb.cmdintepreter.cmd.Interpreter;
import org.dstorerdb.cmdintepreter.interfaces.InterpreterListener;
import org.dstorerdb.common.model.User;
import org.dstorerdb.common.system.ServerMessages;
import org.dstorerdb.server.net.Client;
import org.dstorerdb.server.net.Server;
import org.dstorerdb.server.net.thread.TRequestManager;
import org.dstorerdb.server.system.UserManager;

public class RQMInterpreterListener implements InterpreterListener {

    private final TRequestManager reqManager;
    private final Interpreter requestInterpreter;

    public RQMInterpreterListener(TRequestManager reqManager) {
        this.reqManager = reqManager;
        requestInterpreter = reqManager.getRequestInterpreter();
    }

    private void buildMessage(String header, String msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(header).append('-').append(msg).append(ServerMessages.RESPONSE_MSG);
        requestInterpreter.setResponseMsg(sbMsg.toString());
    }

    @Override
    public void exec(Command cmd) {
        final String CMD_ORDER = cmd.getOrder();
        final int optCount = cmd.getOptionsCount();
        StringBuilder sbMsg = new StringBuilder();

        switch(CMD_ORDER){
            case CmdOrder.CLOSE:
                if (optCount == 0) {
                    buildMessage(ServerMessages.OK_MSG, "Conexion cerrada con exito");
                    reqManager.setConnected(false);
                }
                else
                    buildMessage(ServerMessages.ERROR_OPTION, "Este comando no posee parametros");
                break;

            case CmdOrder.LOGIN:
                if (cmd.hasOptions()) {
                    if (cmd.getOptionsCount() == 2) {
                        String nick = cmd.getOptionAt(0);
                        String passw = cmd.getOptionAt(1);
                        boolean isValid = Server.getServer().getInfoManager()
                                .existsUser(nick, passw);
                        if (isValid) {
                            sbMsg.append(ServerMessages.OK_MSG)
                                    .append('-')
                                    .append("Login correcto!")
                                    .append(ServerMessages.RESPONSE_MSG);
                            requestInterpreter.setResponseMsg(sbMsg.toString());

                            // Crear el cliente con los datos del usuario
                            User loged = new User(nick, passw);
                            Client newCli =
                                    new Client(loged,
                                            reqManager.getSockRequest(),
                                            reqManager.getOutputStream(),
                                            reqManager.getInputStream());
                            Server.getServer().addClient(newCli);
                        }
                        else{
                            // Mas adelante dejar solo el identificador del error
                            // de login
                            sbMsg.append(ServerMessages.ERROR_LOGIN)
                                    .append('-').append("Error de login")
                                    .append(ServerMessages.RESPONSE_MSG);
                            requestInterpreter.setResponseMsg(sbMsg.toString());
                        }
                    }
                    else{
                        sbMsg.append(ServerMessages.ERROR_OPTION)
                                .append('-').append("Cantidad de opciones invalida")
                                .append(ServerMessages.RESPONSE_MSG);
                        requestInterpreter.setResponseMsg(sbMsg.toString());
                    }
                }
                else{
                    sbMsg.append(ServerMessages.ERROR_OPTION)
                            .append('-').append("Comando sin opciones")
                            .append(ServerMessages.RESPONSE_MSG);
                    requestInterpreter.setResponseMsg(sbMsg.toString());
                }
                break;

            case CmdOrder.REGISTER:
                if(cmd.getOptionsCount() == 3) {
                    String nick = cmd.getOptionAt(0);
                    String pass1 = cmd.getOptionAt(1);
                    String pass2 = cmd.getOptionAt(2);
                    if (!pass1.equals(pass2)) {
                        sbMsg.append(ServerMessages.ERROR_REGISTER).append('-')
                                .append("Las contraseñas no coinciden")
                                .append(ServerMessages.RESPONSE_MSG);
                        requestInterpreter.setResponseMsg(sbMsg.toString());
                    }
                    else {
                        pass2 = null;
                        User newUser = new User(nick, pass1);
                        boolean created = UserManager.create(newUser);
                        sbMsg.append(created ? ServerMessages.OK_MSG : ServerMessages.ERROR_REGISTER)
                                .append('-').append(created ? "Usuario creado con exito" : "Nick ya existente")
                                .append(ServerMessages.RESPONSE_MSG);
                        requestInterpreter.setResponseMsg(sbMsg.toString());
                    }
                }
                else {
                    sbMsg.append(ServerMessages.ERROR_OPTION).append('-')
                            .append("Cantidad de opciones invalida")
                            .append(ServerMessages.RESPONSE_MSG);
                    requestInterpreter.setResponseMsg(sbMsg.toString());
                }

                break;

            default:
                if (CmdOrder.isAvailableOrder(CMD_ORDER))
                    sbMsg.append(ServerMessages.GENERAL_ERROR)
                            .append('-').append("Comando no disponible para su uso");
                else
                    sbMsg.append(ServerMessages.ERROR_ORDER)
                            .append('-').append("Comando desconocido");
                sbMsg.append(ServerMessages.RESPONSE_MSG);
                requestInterpreter.setResponseMsg(sbMsg.toString());
                sbMsg = null;
                break;
        }
    }
}

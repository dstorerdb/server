/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.system;

import org.dstorerdb.cmdintepreter.cmd.CmdOption;
import org.dstorerdb.common.system.NumberID;
import org.dstorerdb.common.system.ServerMessages;
import org.dstorerdb.common.system.SysInfo;
import org.mpizutil.electrolist.structure.ElectroList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public class DataList {

    private File fileList;
    private String name;
    private ElectroList listData;
    private BufferedWriter buffFileWriter; 

    public DataList(File parent, String name){
        this(new File(parent, name));
    }
    
    public DataList(File fileList) {
        this(fileList, false);
    }
    
    public DataList(File fileList, boolean create) {
        try {
            this.fileList = fileList;
            if (create && !fileList.exists())
                fileList.createNewFile();
            this.name = fileList.getName();
            listData = new ElectroList();
            buffFileWriter = new BufferedWriter(new FileWriter(fileList, true));
            loadList();
        } catch (IOException ex) {
            System.out.println("Lista no creada anteriormente");
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DataList(String filePath) {
        this(new File(filePath));
    }

    public DataList(File folderUser, String listName, UserManager.OPEN_LIST_OPTION openOption) {
        this(new File(folderUser, listName), openOption == UserManager.OPEN_LIST_OPTION.CREATE);
    }
    
    /**
     * Devuelve el objeto casteado por su tipo basandose en su representacion textual, 
     * es decir si el objeto estaa representado por 1I por ejemplo, el metodo devolvera
     * un int con el valor de este objeto entregado.
     * @param strObj Representacion textual del objeto a convertir
     * @return Objeto transformado en su tipo correspondiente
     */
    private Object getCastedObject(String strObj){
        
        if ((strObj.startsWith("'") && strObj.endsWith("'")) || 
                (strObj.startsWith("\"") && strObj.endsWith("\"")))
            return strObj.substring(1, strObj.length()-1);
        else{
            final int strLen = strObj.length();
            final char numId = strObj.charAt(strLen-1);
            switch(numId){
                case NumberID.LONG:
                    return Long.parseLong(strObj.substring(0, strLen-1));
                case NumberID.INT:
                    return Integer.parseInt(strObj.substring(0, strLen-1));
                case NumberID.SHORT:
                    return Short.parseShort(strObj.substring(0, strLen-1));
                case NumberID.BYTE:
                    return Byte.parseByte(strObj.substring(0, strLen-1));
                case NumberID.FLOAT:
                    return Float.parseFloat(strObj.substring(0, strLen-1));
                case NumberID.DOUBLE:
                    return Double.parseDouble(strObj.substring(0, strLen-1));
                default:
                    return null;
            }
        }
    }
    
    /**
     * Devuelve un String representando el formato del objeto segun su tipo,
     * es decir, si el objeto por ejemplo es un int se devuelve un valor ObjI
     * o si es un String este es devuelto entre comilla simple
     * @param obj Objeto a convertir
     * @return Objeto convertido en formato texto
     */
    private String getCastedString(Object obj){
        if (obj instanceof String)
            return '\''+obj.toString()+'\'';
        else{
            final String className = obj.getClass().getSimpleName().toLowerCase();
            switch(className){
                case DataType.LONG:
                    return obj.toString()+NumberID.LONG;
                case "integer":
                    return obj.toString()+NumberID.INT;
                case DataType.INT:
                    return obj.toString()+NumberID.INT;
                case DataType.SHORT:
                    return obj.toString()+NumberID.SHORT;
                case DataType.BYTE:
                    return obj.toString()+NumberID.BYTE;
                case DataType.FLOAT:
                    return obj.toString()+NumberID.FLOAT;
                case DataType.DOUBLE:
                    return obj.toString()+NumberID.DOUBLE;
                default:
                    return null;
            }
        }
    }
    
    private String[] getFileElements() throws IOException{
        List<String> fileLines = Files.readAllLines(fileList.toPath());
        if (fileLines.isEmpty())
            return null;
        else{
            StringBuilder sbLines = new StringBuilder();
            for (String line : fileLines)
                sbLines.append(line);
            return sbLines.toString().split(SysInfo.ELEMENTS_SEPARATOR);
        }
    }
    
    private void saveList(){
        try {
            // Mas adelante probar con Files.write
            buffFileWriter.close();
            buffFileWriter = new BufferedWriter(new FileWriter(fileList));
            StringBuilder sbObjects = new StringBuilder();
            
            for (Object object : listData) {
                sbObjects.append(getCastedString(object));
                sbObjects.append(SysInfo.ELEMENTS_SEPARATOR);
            }
            sbObjects.deleteCharAt(sbObjects.length()-1);
            sbObjects.deleteCharAt(sbObjects.length()-1);
            buffFileWriter.write(sbObjects.toString());
            buffFileWriter.flush();
            sbObjects = null;
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadList() throws IOException{
        String[] splitElements = getFileElements();
        if (splitElements == null)
            return;

        int splitLen = splitElements.length;
        for (int i = 0; i < splitLen; i++)
            listData.add(getCastedObject(splitElements[i]));
        splitElements = null;
    }
    
    private void writeData(String data){
        try {
            if (!listData.isEmpty())
                buffFileWriter.write(SysInfo.ELEMENTS_SEPARATOR);
            buffFileWriter.write(data);
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private int getSizeOf(Object obj){
        if (obj instanceof String)
            return obj.toString().length();
        else{
            try {
                Field sizeField = obj.getClass().getDeclaredField("SIZE");
                return Integer.parseInt(sizeField.get(obj).toString())/8;
            } catch (NoSuchFieldException | SecurityException
                    | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
            }
            return -1;
        }
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name){
        try {
            this.name = name;
            buffFileWriter.close();
            fileList.renameTo(new File(fileList.getParentFile(), name));
            buffFileWriter = new BufferedWriter(new FileWriter(fileList, true));
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ElectroList getListData() {
        return listData;
    }
    
    // Crear metodos para la ejecucion de los comandos

    public File getFileList() {
        return fileList;
    }
    
    public void add(String data){
        try {
            Object obj = getCastedObject(data);
            writeData(data);
            buffFileWriter.flush();
            listData.add(obj);
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void add(int index, String data){
        Object obj = getCastedObject(data);
        listData.add(index, obj);
        saveList();
    }
    
    public void add(String... data){
        try {
            String sing;
            
            for (int i = 0; i < data.length; i++) {
                sing = data[i];
                listData.add(getCastedObject(sing));
                writeData(sing);
            }
            buffFileWriter.flush();
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void add(int index, String... data){
        final int dataLen = data.length;
        for (int i = 0; i < dataLen; i++)
            listData.add(index+i, data[i]);
        saveList();
    }
    
    public void clearAll(){
        try {
            listData.clear();
            buffFileWriter.close();
            buffFileWriter = new BufferedWriter(new FileWriter(fileList));
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete() {
        try {
            listData.clear();
            buffFileWriter.close();
            fileList.delete();
        } catch (IOException ex) {
            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean deleteInIndex(int index){
        try {
            listData.remove(0);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    } 
    
    public boolean deleteIf(String filter, String symbol){
        boolean isRemoved = false;
        if (symbol.equals(CmdOption.EQUALS)){
            try {
                isRemoved = listData.removeIf(e->e.equals(filter));
                buffFileWriter.close();
                buffFileWriter = new BufferedWriter(new FileWriter(fileList));
                saveList();
            } catch (IOException ex) {
                Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            try {
                isRemoved = listData.removeIf(e->e.toString().contains(filter));
                buffFileWriter.close();
                buffFileWriter = new BufferedWriter(new FileWriter(fileList));
                saveList();
            } catch (IOException ex) {
                Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return isRemoved;
    }
    
    public int getSizeOf(){
        int sizeOf = 0;
        for (Object object : listData)
            sizeOf+=getSizeOf(object);
        return sizeOf;
    }
    
    public String getAll(){
        if (listData.isEmpty())
            return ServerMessages.EMPTY;
        StringBuilder sbList = new StringBuilder();
        
        for (Object object : listData)
            sbList.append(object.toString()).append(SysInfo.ELEMENTS_SEPARATOR);
        
        return sbList.substring(0, sbList.length()-2);
    }
    
    public String getByRange(int start, int end){
        if (start >= listData.size() || end >= listData.size())
            return ServerMessages.BAD_RANGE;
        if(listData.isEmpty())
            return ServerMessages.EMPTY;
        
        if (start == end)
            return listData.get(end).toString();
        
        StringBuilder sbData = new StringBuilder();
        ElectroList<String> listResults = listData.get(start, end);
    
        for (String result : listResults)
            sbData.append(result).append(SysInfo.ELEMENTS_SEPARATOR);
        return sbData.substring(0, sbData.length()-2);
    }
    
    public String getByIndex(int index){
        if(index < 0 || index >= listData.size())
            return ServerMessages.BAD_RANGE;
        if(listData.isEmpty())
            return ServerMessages.EMPTY;
        return listData.get(index).toString();
    }

    public void setAllTo(String data){
        final int numAdd = listData.size();
        listData.clear();
        for (int i = 0; i < numAdd; i++)
            listData.add(getCastedObject(data));
        saveList();
    }
    
    public boolean setElement(int index, String newElement){
        if (listData.isEmpty() || listData.size() >= index)
            return false;
        listData.set(index, newElement);
        saveList();
        return true;
    }
    
}

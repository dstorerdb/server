package org.dstorerdb.server.system;

public enum LogMessage {
    SERVER_STARTED,
    WAITING_CLIENTS,
    CLIENT_CONNECTED_FROM,
    REQUEST_FROM_CLIENT,
    RESPONSE_CLIENT,
    TCLIENT_REQUEST,
    TCLIENT_RESPONSE
}

package org.dstorerdb.server.system;

import org.dstorerdb.common.system.SysInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class MessagesManager {
    private final File fileProps;
    private final Properties props;

    private static final MessagesManager msgManager = new MessagesManager();

    public static MessagesManager getInstance() {
        return msgManager;
    }

    private MessagesManager() {
        fileProps = new File(SysInfo.DEFAULT_ROOT_DIR, "log_messages.xml");
        props = new Properties();

        if (fileProps.exists()) {
            loadData();
            try {
                System.out.println(fileProps.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                fileProps.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            createDefaultData();
            saveData();
        }
    }

    public String getProperty(LogMessage message) {
        return props.getProperty(message.name());
    }

    public void setPropery(LogMessage message, String value) {
        props.setProperty(message.name(), value);
        saveData();
    }

    private void createDefaultData() {
        props.setProperty(LogMessage.SERVER_STARTED.name(), "Server started");
        props.setProperty(LogMessage.WAITING_CLIENTS.name(), "Waiting clients...");
        props.setProperty(LogMessage.CLIENT_CONNECTED_FROM.name(), "Client connected from: ");
        props.setProperty(LogMessage.REQUEST_FROM_CLIENT.name(), "Request from client ");
        props.setProperty(LogMessage.RESPONSE_CLIENT.name(), "Response: ");
        //props.setProperty(LogMessage.TCLIENT_REQUEST.name(), "Request: ");
        //props.setProperty(LogMessage.TCLIENT_RESPONSE.name(), "Response: ");
    }

    private void saveData() {
        try {
            props.storeToXML(new FileOutputStream(fileProps), "Log Messages");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        try {
            props.loadFromXML(new FileInputStream(fileProps));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.config;

import org.dstorerdb.common.system.SysInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public final class ConfigManager {
    private final File fileConf;
    private final Properties proper;

    public ConfigManager() {
        fileConf = new File(SysInfo.DEFUALT_PATH, "config.xml");
        proper = new Properties();
        if (fileConf.exists())
            loadChanges();
        else
            saveChanges();
    }

    public ConfigManager(File fileConf) {
        this.fileConf = fileConf;
        proper = new Properties();
        if (fileConf.exists())
            loadChanges();
        else
            saveChanges();
    }

    public String getConfigValue(String name){
        return proper.getProperty(name);
    }
    
    public void setConfigValue(String name, String value){
        proper.setProperty(name, value);
        saveChanges();
    }
    
    public void saveChanges(){
        try {
            proper.store(new FileOutputStream(fileConf), "DataStorer Config File");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadChanges(){
        try {
            proper.load(new FileInputStream(fileConf));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

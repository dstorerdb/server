/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.server.config;

import org.dstorerdb.common.system.SysInfo;
import org.mpizutil.electrolist.structure.ElectroList;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author martin
 */
public class InfoManager {
    private final File sysUsersDirs;

    private static final FileFilter userDirFilter = pathname -> pathname.isDirectory();
    
    public InfoManager() {
        this.sysUsersDirs = SysInfo.DEFAULT_USERS_DIR;
    }

    private File[] getUserDirs(){
        return sysUsersDirs.listFiles(userDirFilter);
    }
    
    public int getUsersCount(){
        File[] listFiles = getUserDirs();
        return listFiles == null ? 0 : listFiles.length;
    }

    public boolean existsUser(String nick, String passw){
        File[] userDirFilter = sysUsersDirs.listFiles(
                (File pathname) -> pathname.isDirectory() && pathname.getName().equals(nick));

        if (userDirFilter == null || userDirFilter.length == 0)
            return false;
        //final File userDir = userDirFilter[0];
        final File configFile = getConfigFileFrom(userDirFilter[0]);
        // El configManager es una clase generica que me permite rescatar configuraciones
        // de cualquier archivo xml dedicado para ello
        ConfigManager configManager = new ConfigManager(configFile);
        String realNick = configManager.getConfigValue("nick");
        String realPassw = configManager.getConfigValue("passw");
        return nick.equals(realNick) && passw.equals(realPassw);
    }
    
    private File getConfigFileFrom(File userDir){
        final File[] dirFiles = userDir.listFiles(
                pathname -> !pathname.isDirectory() && pathname.getName()
                .equals(SysInfo.CONFIG_NAME_FILE));
        return dirFiles == null || dirFiles.length == 0 ? null : dirFiles[0];
    }
    
    public ElectroList<String> getUserNames(){
        final File[] userDirs = getUserDirs();
        ElectroList<String> listUserNames = new ElectroList<>();
        
        if (userDirs != null) {
            final int udLen = userDirs.length;
            for (int i = 0; i < udLen; i++)
                listUserNames.add(userDirs[i].getName());
        }
        return listUserNames;
    }
    
}
